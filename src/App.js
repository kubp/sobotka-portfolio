import React, { Component } from 'react';
// Create a Title component that'll render an <h1> tag with some styles
import styled, { keyframes } from 'styled-components';

const imageAnimation = keyframes`
  0% { 
    transform: translateY(0);  opacity: 0;
  }
  
  10% { opacity: 1; }
  
  90% {
    opacity: 1;
  }
  100% { 
    transform: translateY(-3.8%); opacity: 0; 
  }
`;

const textAnimation = keyframes`
  0% { 
    transform: translateY(3.8%); 
    opacity: 0
  }
  5% { 
    transform: translateY(0); 
    opacity: 1
  }
  90% { 
    transform: translateY(0px); 
    opacity: 1;
  }
  100% { 
    transform: translateY(-10px); 
    opacity: 0; 
  }
`;

const Wrapped = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  -webkit-box-pack: center;
  justify-content: center;
`;

const Image = styled.div`
  height: 120%;
  width: 100%;
  background: ${({ image }) => `url(${image}) repeat-y`};
  background-size: cover;
  background-position: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  animation: ${imageAnimation} 8s linear infinite;
`;

const Heading = styled.h1`
  font-size: 40px;
  font-weight: 900;

  z-index: 5000;
  color: rgb(255, 255, 255);
  max-width: 767px;
  margin: 0 auto;
  transform: translateY(0px);
  animation: ${textAnimation} 8s cubic-bezier(0.65, 0.05, 0.36, 1) infinite;

  @media only screen and (max-width: 760px) {
    font-size: 30px;
    padding: 20px;
  }
`;

const Header = styled.div`
  position: absolute;
  right: 20px;
  top: 20px;
  display: flex;
  width: 275px;
  justify-content: space-between;
`;

const Button = styled.button`
  width: 132px;
  height: 36px;
  border-radius: 3px;
  background-color: #16a348;
  z-index: 1000;
  border: 0px;
  font-size: 14px;
  font-weight: bold;
  color: #ffffff;
  cursor: pointer;
`;

const GreyButton = styled(Button)`
  background-color: rgba(152, 152, 152, 0.5294117647058824);
`;

const Logo = styled.div`
  position: absolute;
  left: 20px;
  top: 20px;
  width: 50px;
  z-index: 100;
  img {
    width: 64px;
    height: 19px;
  }
`;

const resources = [
  {
    text: 'I focus on the users’ needs and a business goals.',
    img: '/01jakub.jpg',
  },
  {
    text:
      'I’m trying to bring a value with intuitive and frustration-free products.',
    img: '/02jakub.jpg',
  },
  {
    text: 'I have been desining for more than +7000 working hours.',
    img: '/03jakub.jpg',
  },
  {
    text: 'Being a designer means solving problems for me.',
    img: '/04jakub.jpg',
  },
  {
    text:
      'My mindset is always stay the same: I research, I prototype, I test and iterate. ',
    img: '/05jakub.jpg',
  },
  {
    text: 'I’m not afraid to fight against dark patterns and manipulations.',
    img: '/06jakub.jpg',
  },
  {
    text: 'I believe that There is More, I’m involved in local Church in Brno.',
    img: '/07jakub.jpg',
  },
  {
    text: 'Let’s team up if you’re up to something.',
    img: '/08jakub.jpg',
  },
];

class Inside extends Component {
  render() {
    console.log('rerender');
    return (
      <Wrapped className="App">
        <Image image={this.props.res.img} />
        <Heading>{this.props.res.text}</Heading>
      </Wrapped>
    );
  }
}

class App extends Component {
  state = { position: 0 };

  componentDidMount() {
    setInterval(
      () => this.setState({ position: this.state.position + 1 }),
      8000,
    );
  }

  render() {
    return (
      <div>
        <Logo>
          <img src={'./logo.png'} alt={'logo'} />
        </Logo>
        <Header>
          <GreyButton>LinkedIn</GreyButton>
          <Button>Download CV</Button>
        </Header>
        <Inside res={resources[this.state.position % resources.length]} />
      </div>
    );
  }
}

export default App;
